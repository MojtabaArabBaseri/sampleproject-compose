package ir.millennium.sampleprojectcompose.domain.useCase

import androidx.paging.Pager
import ir.millennium.sampleprojectcompose.domain.entity.ArticleEntity
import javax.inject.Inject

open class GetArticlesUseCase @Inject constructor(private val pager: Pager<Int, ArticleEntity>) {
    operator fun invoke() = pager.flow
}