package ir.millennium.sampleprojectcompose.presentation.utils

import android.R
import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Typeface
import android.util.TypedValue
import android.widget.ImageView
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.navigation.NavController
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import kotlin.math.roundToInt

fun convertDpToPx(context: Context, dp: Int): Int {
    val r = context.resources
    return TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP,
        dp.toFloat(),
        r.displayMetrics
    ).roundToInt()
}

@Composable
fun LazyListState.isScrollingUp(): Boolean {
    var previousIndex by remember(this) { mutableIntStateOf(firstVisibleItemIndex) }
    var previousScrollOffset by remember(this) { mutableIntStateOf(firstVisibleItemScrollOffset) }
    return remember(this) {
        derivedStateOf {
            if (firstVisibleItemIndex < 3) {
                true
            } else {
                if (previousIndex != firstVisibleItemIndex) {
                    previousIndex > firstVisibleItemIndex
                } else {
                    previousScrollOffset >= firstVisibleItemScrollOffset
                }.also {
                    previousIndex = firstVisibleItemIndex
                    previousScrollOffset = firstVisibleItemScrollOffset
                }
            }
        }
    }.value
}

fun ViewPager2.reduceDragSensitivity() {
    val recyclerViewField = ViewPager2::class.java.getDeclaredField("mRecyclerView")
    recyclerViewField.isAccessible = true
    val recyclerView = recyclerViewField.get(this) as RecyclerView

    val touchSlopField = RecyclerView::class.java.getDeclaredField("mTouchSlop")
    touchSlopField.isAccessible = true
    val touchSlop = touchSlopField.get(recyclerView) as Int
    touchSlopField.set(recyclerView, (touchSlop * 3.5).toInt())  // "8" was obtained experimentally
}

private fun isColorLight(color: Int): Boolean {
    val lightness =
        0.299 * Color.red(color) + 0.587 * Color.green(color) + 0.114 * Color.blue(color)
//    return lightness > 127
    return lightness > 150
}

fun Activity.textAsBitmap(text: String?, textSize: Float, textColor: Int): Bitmap {
    val paint = Paint(Paint.ANTI_ALIAS_FLAG)
    paint.textSize = 200F
    paint.color = resources.getColor(R.color.white)
    paint.textAlign = Paint.Align.LEFT
    val baseline = -paint.ascent() // ascent() is negative
    val width = (paint.measureText(text) + 0.0f).toInt() // round
    val height = (baseline + paint.descent() + 0.0f).toInt()
    val image = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
    val canvas = Canvas(image)
    canvas.drawText(text!!, 0f, baseline, paint)
    return image
}

fun ImageView.setTextBitmap(text: String, textSize: Float, textColor: Int, typeface: Typeface) {
    val paint = Paint(Paint.ANTI_ALIAS_FLAG)
    paint.textSize = textSize
    paint.color = textColor
    paint.textAlign = Paint.Align.LEFT
    paint.typeface = typeface
    val lines = text.split("\n")
    var maxWidth = 0
    for (line in lines) {
        val width = paint.measureText(line).toInt()
        if (width > maxWidth) {
            maxWidth = width
        }
    }
    val height = paint.descent() - paint.ascent()
    val bitmap = Bitmap.createBitmap(maxWidth, height.toInt() * lines.size, Bitmap.Config.ARGB_8888)
    val canvas = Canvas(bitmap)
    var y = -paint.ascent()
    for (line in lines) {
        canvas.drawText(line, 0f, y, paint)
        y += height
    }
    setImageBitmap(bitmap)
}

fun NavController.changeStartDestination(
    graphId: Int,
    startDestinationId: Int
) {
    val navGraph = navInflater.inflate(graphId)
    navGraph.setStartDestination(startDestinationId)
    graph = navGraph
}