package ir.millennium.sampleprojectcompose.presentation.activity.mainActivity

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import ir.millennium.sampleprojectcompose.data.dataSource.local.preferencesDataStoreManager.UserPreferencesRepository
import ir.millennium.sampleprojectcompose.domain.entity.TypeLanguage
import ir.millennium.sampleprojectcompose.domain.entity.TypeTheme
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

@HiltViewModel
open class MainActivityViewModel @Inject constructor(
    val userPreferencesRepository: UserPreferencesRepository
) : ViewModel() {

    private val statusThemeFlow = userPreferencesRepository.statusTheme
    private val _typeTheme = runBlocking { MutableStateFlow(statusThemeFlow.first()) }
    val typeTheme: StateFlow<Int> = _typeTheme

    private val languageAppFlow = userPreferencesRepository.languageApp
    private val _languageApp = runBlocking { MutableStateFlow(languageAppFlow.first()) }
    val languageApp: StateFlow<String> = _languageApp

    private val _authScreen = MutableStateFlow(true)
    val authScreen: StateFlow<Boolean> = _authScreen

    suspend fun onThemeChanged(newTheme: Int) {
        when (newTheme) {
            TypeTheme.LIGHT.typeTheme -> {
                userPreferencesRepository.setStatusTheme(TypeTheme.LIGHT.typeTheme)
                _typeTheme.value = TypeTheme.LIGHT.typeTheme
            }

            TypeTheme.DARK.typeTheme -> {
                userPreferencesRepository.setStatusTheme(TypeTheme.DARK.typeTheme)
                _typeTheme.value = TypeTheme.DARK.typeTheme
            }
        }
    }

    suspend fun onLanguageChanged(newLanguage: String) {
        when (newLanguage) {
            TypeLanguage.ENGLISH.typeLanguage -> {
                userPreferencesRepository.setLanguageApp(TypeLanguage.ENGLISH.typeLanguage)
                _languageApp.value = TypeLanguage.ENGLISH.typeLanguage
            }

            TypeLanguage.PERSIAN.typeLanguage -> {
                userPreferencesRepository.setLanguageApp(TypeLanguage.PERSIAN.typeLanguage)
                _languageApp.value = TypeLanguage.PERSIAN.typeLanguage
            }
        }
    }

    fun onAuthScreen(authScreen: Boolean) {
        this._authScreen.value = authScreen
    }
}