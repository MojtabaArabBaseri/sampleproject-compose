package ir.millennium.sampleprojectcompose.data.dataSource.local.database

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Query
import androidx.room.Upsert
import ir.millennium.sampleprojectcompose.domain.entity.ArticleEntity

@Dao
interface ArticleDao {

    @Upsert
    suspend fun upsertAll(articles: List<ArticleEntity>)

    @Query("SELECT * FROM tbl_Article")
    fun pagingSource(): PagingSource<Int, ArticleEntity>

    @Query("DELETE FROM tbl_Article")
    suspend fun clearAll()
}