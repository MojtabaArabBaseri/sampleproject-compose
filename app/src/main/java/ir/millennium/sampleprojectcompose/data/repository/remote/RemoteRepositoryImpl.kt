package ir.millennium.sampleprojectcompose.data.repository.remote

import ir.millennium.sampleprojectcompose.data.dataSource.remote.ApiService
import ir.millennium.sampleprojectcompose.domain.repository.remote.RemoteRepository

class RemoteRepositoryImpl(private val apiService: ApiService) : RemoteRepository