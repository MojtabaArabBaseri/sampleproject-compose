package ir.millennium.sampleprojectcompose.data.mapper

import ir.millennium.sampleprojectcompose.data.model.remote.ArticleItem
import ir.millennium.sampleprojectcompose.domain.entity.ArticleEntity

fun ArticleEntity.mapToArticleItem() =
    ArticleItem(
        title = this.title,
        publishedAt = this.publishedAt,
        author = this.author,
        urlToImage = this.urlToImage,
        description = this.description,
        url = this.url,
        content = this.content
    )

fun ArticleItem.mapToArticleEntity() =
    ArticleEntity(
        title = this.title,
        publishedAt = this.publishedAt,
        author = this.author,
        urlToImage = this.urlToImage,
        description = this.description,
        url = this.url,
        content = this.content
    )
