package ir.millennium.sampleprojectcompose.data.repository.local

import ir.millennium.sampleprojectcompose.data.dataSource.local.database.AppDatabase
import ir.millennium.sampleprojectcompose.domain.repository.local.LocalRepository
import kotlinx.coroutines.CoroutineScope
import javax.inject.Inject

class LocalRepositoryImpl @Inject constructor(
    private var database: AppDatabase,
    private val applicationScope: CoroutineScope
) : LocalRepository