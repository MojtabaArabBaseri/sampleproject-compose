package ir.millennium.sampleprojectcompose.data.dataSource.local.database

import androidx.room.Database
import androidx.room.RoomDatabase
import ir.millennium.sampleprojectcompose.domain.entity.ArticleEntity

@Database(entities = [ArticleEntity::class], version = 1, exportSchema = true)
abstract class AppDatabase : RoomDatabase() {

    abstract fun articleDao(): ArticleDao

}

